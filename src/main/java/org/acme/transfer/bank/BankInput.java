package org.acme.transfer.bank;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class BankInput {
    private UUID id;
    private String name;
    private String country;
}