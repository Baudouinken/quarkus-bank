package org.acme.transfer.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserInput {
    private UUID id;
    private String firstName;
    private String lastName;
    private int age;
    private String address;
    private String country;
    private String city;
}