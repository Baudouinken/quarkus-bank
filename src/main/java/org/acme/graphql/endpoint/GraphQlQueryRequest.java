package org.acme.graphql.endpoint;

import jakarta.validation.constraints.NotNull;

import java.util.Map;

public class GraphQlQueryRequest {
    @NotNull(message = "query must not be null")
    private String query;

    private Map<String, Object> variables;

    public GraphQlQueryRequest() {
    }

    public GraphQlQueryRequest(@NotNull(message = "query must not be null") String query) {
        super();
        this.query = query;
    }

    public GraphQlQueryRequest(String query, Map<String, Object> variables) {
        this.query = query;
        this.variables = variables;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Map<String, Object> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, Object> variables) {
        this.variables = variables;
    }
}
