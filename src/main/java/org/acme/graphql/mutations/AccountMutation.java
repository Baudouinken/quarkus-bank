package org.acme.graphql.mutations;

import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.acme.models.Account;
import org.acme.models.Bank;
import org.acme.models.User;
import org.acme.repositories.AccountRepository;
import org.acme.repositories.BankRepository;
import org.acme.repositories.UserRepository;
import org.acme.transfer.account.AccountInput;
import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Mutation;
import org.eclipse.microprofile.graphql.Name;

import java.util.UUID;

@GraphQLApi
public class AccountMutation {

    @Inject
    private AccountRepository accountRepository;
    @Inject
    private BankRepository bankRepository;
    @Inject
    private UserRepository userRepository;

    @Mutation("createAccount")
    @Transactional
    public Account createAccount(@Name("account") AccountInput account) {
        User user = userRepository.findById(account.getUserId());
        Bank bank = bankRepository.findById(account.getBankId());

        var accountToSave = new Account(
                null,
                account.getName(),
                account.getAlias(),
                account.getAmount(),
                user,
                bank
        );
        accountRepository.persistAndFlush(accountToSave);
        return accountToSave;
    }

    @Mutation("updateAccount")
    @Transactional
    public Account updateAccount(@Name("account") AccountInput account) {

        User user = userRepository.findById(account.getUserId());
        Bank bank = bankRepository.findById(account.getBankId());

        var accountToSave = accountRepository.findById(account.getId());

        accountToSave.setAlias(account.getAlias());
        accountToSave.setName(account.getName());
        accountToSave.setAmount(account.getAmount());
        accountToSave.setBank(bank);
        accountToSave.setUser(user);

        accountRepository.persistAndFlush(accountToSave);
        return accountToSave;
    }

    @Mutation("deleteAccount")
    @Transactional
    public UUID deleteAccount(@Name("accountId") UUID accountId) {
        accountRepository.deleteById(accountId);
        return accountId;
    }
}