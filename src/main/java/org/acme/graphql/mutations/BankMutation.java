package org.acme.graphql.mutations;

import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.acme.models.Bank;
import org.acme.repositories.BankRepository;
import org.acme.transfer.bank.BankInput;
import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Mutation;
import org.eclipse.microprofile.graphql.Name;

import java.util.UUID;

@GraphQLApi
public class BankMutation {
    
    @Inject
    private BankRepository bankRepository;

    @Mutation("createBank")
    @Transactional
    public Bank createBank(@Name("bank") BankInput bank) {

        var bankToSave = new Bank();
        bankToSave.setName(bank.getName());
        bankToSave.setCountry(bank.getCountry());        

        bankRepository.persistAndFlush(bankToSave);

        return bankToSave;
    }

    @Mutation("updateBank")
    @Transactional
    public Bank updateBank(@Name("bank") BankInput bank) {
               
        var bankToSave = bankRepository.findById(bank.getId());

        bankToSave.setName(bank.getName());
        bankToSave.setCountry(bank.getCountry());

        bankRepository.persistAndFlush(bankToSave);

        return bankToSave;
    }

    @Mutation("deleteBank")
    @Transactional
    public UUID deleteBank(@Name("bankId") UUID bankId) {
        bankRepository.deleteById(bankId);
        return bankId;
    }
    
}