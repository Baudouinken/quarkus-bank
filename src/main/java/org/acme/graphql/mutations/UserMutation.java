package org.acme.graphql.mutations;

import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.acme.models.User;
import org.acme.repositories.UserRepository;
import org.acme.transfer.user.UserInput;
import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Mutation;
import org.eclipse.microprofile.graphql.Name;

import java.util.UUID;

@GraphQLApi
public class UserMutation {

    @Inject
    private UserRepository userRepository;

    @Mutation("createUser")
    @Transactional
    public User createUser(@Name("user") UserInput user) {

        var userToSave = new User();
        userToSave.setAge(user.getAge());
        userToSave.setFirstName(user.getFirstName());
        userToSave.setLastName(user.getLastName());
        userToSave.setCity(user.getCity());
        userToSave.setAddress(user.getAddress());
        userToSave.setCountry(user.getCountry());

        userRepository.persistAndFlush(userToSave);

        return userToSave;
    }

    @Mutation("updateUser")
    @Transactional
    public User updateUser(@Name("user") UserInput user) {

        var userToSave = userRepository.findById(user.getId());

        userToSave.setAge(user.getAge());
        userToSave.setFirstName(user.getFirstName());
        userToSave.setLastName(user.getLastName());
        userToSave.setCity(user.getCity());
        userToSave.setAddress(user.getAddress());
        userToSave.setCountry(user.getCountry());

        userRepository.persistAndFlush(userToSave);

        return userToSave;
    }

    @Mutation("deleteUser")
    @Transactional
    public UUID deleteUser(@Name("userId") UUID userId) {
        userRepository.deleteById(userId);
        return userId;
    }

}