package org.acme.graphql.queries;

import jakarta.inject.Inject;
import org.acme.models.Account;
import org.acme.repositories.AccountRepository;
import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Name;
import org.eclipse.microprofile.graphql.Query;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@GraphQLApi
public class AccountQuery {

    @Inject
    private AccountRepository repository;
    @Query("accounts")
    public List<Account> findAll() {
        return repository.findAll().stream().collect(Collectors.toList());
    }
    @Query("account")
    public Account findById(@Name("accountId") UUID id) {
        return repository.findById(id);
    }
}