package org.acme.graphql.queries;

import jakarta.inject.Inject;
import org.acme.models.Bank;
import org.acme.repositories.BankRepository;
import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Name;
import org.eclipse.microprofile.graphql.Query;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@GraphQLApi
public class BankQuery {

    @Inject
    private BankRepository repository;
    @Query("banks")
    public List<Bank> findAll() {
        return repository.findAll().stream().collect(Collectors.toList());
    }
    @Query("bank")
    public Bank findById(@Name("bankId") UUID id) {
        return repository.findById(id);
    }
}