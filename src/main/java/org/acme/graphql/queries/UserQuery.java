package org.acme.graphql.queries;

import jakarta.inject.Inject;
import org.acme.models.User;
import org.acme.repositories.UserRepository;
import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Name;
import org.eclipse.microprofile.graphql.Query;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@GraphQLApi
public class UserQuery {

    @Inject
    private UserRepository repository;
    @Query("users")
    public List<User> findAll() {
        return repository.findAll().stream().collect(Collectors.toList());
    }
    @Query("user")
    public User findById(@Name("userId") UUID id) {
        return repository.findById(id);
    }
}