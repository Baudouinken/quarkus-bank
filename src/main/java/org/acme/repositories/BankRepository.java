package org.acme.repositories;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import jakarta.enterprise.context.ApplicationScoped;
import org.acme.models.Bank;

import java.util.UUID;

@ApplicationScoped
public class BankRepository implements PanacheRepositoryBase<Bank, UUID> {
}