package org.acme;

import org.acme.models.Account;
import org.acme.models.Bank;
import org.acme.models.User;
import org.acme.transfer.account.AccountInput;
import org.acme.transfer.bank.BankInput;
import org.acme.transfer.user.UserInput;

import java.math.BigDecimal;
import java.util.Random;
import java.util.UUID;

public class ItBase {

    public BankInput buildBankInput() {
        BankInput bankInput = new BankInput();
        bankInput.setName(UUID.randomUUID().toString());
        bankInput.setCountry(UUID.randomUUID().toString());
        return bankInput;
    }

    public Bank buildBank() {
        Bank bank = new Bank();
        bank.setName(UUID.randomUUID().toString());
        bank.setCountry(UUID.randomUUID().toString());
        return bank;
    }

    public UserInput buildUserInput() {
        UserInput userInput = new UserInput();
        userInput.setFirstName(UUID.randomUUID().toString());
        userInput.setLastName(UUID.randomUUID().toString());
        userInput.setAge(new Random().nextInt());
        userInput.setAddress(UUID.randomUUID().toString());
        userInput.setCountry(UUID.randomUUID().toString());
        userInput.setCity(UUID.randomUUID().toString());
        return userInput;
    }

    public User buildUser() {
        User user = new User();
        user.setFirstName(UUID.randomUUID().toString());
        user.setLastName(UUID.randomUUID().toString());
        user.setAge(new Random().nextInt());
        user.setAddress(UUID.randomUUID().toString());
        user.setCountry(UUID.randomUUID().toString());
        user.setCity(UUID.randomUUID().toString());
        return user;
    }

    public AccountInput buildAccountInput(UUID bankId, UUID userId) {
        AccountInput accountInput = new AccountInput();
        accountInput.setName(UUID.randomUUID().toString());
        accountInput.setAlias(UUID.randomUUID().toString());
        accountInput.setAmount(BigDecimal.valueOf(0));
        accountInput.setBankId(bankId);
        accountInput.setUserId(userId);
        return accountInput;
    }

    public Account buildAccount(Bank bank, User user) {
        Account account = new Account();
        account.setName(UUID.randomUUID().toString());
        account.setAlias(UUID.randomUUID().toString());
        account.setAmount(BigDecimal.valueOf(130));
        account.setBank(bank);
        account.setUser(user);
        return account;
    }

}
