package org.acme.graphql.queries;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import jakarta.inject.Inject;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.transaction.Transactional;
import org.acme.ItBase;
import org.acme.graphql.endpoint.GraphQlQueryRequest;
import org.acme.models.User;
import org.acme.repositories.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsInAnyOrder;

@QuarkusTest
public class QueriesUserIT extends ItBase {

    private User user, user1;

    @Inject
    private UserRepository userRepository;


    @BeforeEach
    @Transactional
    public void setup() {

        user = buildUser();
        user1 = buildUser();
        userRepository.persist(user, user1);
    }

    @AfterEach
    @Transactional
    public void cleanUp() {
        userRepository.deleteAll();
    }

    @Test
    public void users() {

        Map<String, Object> variables = new HashMap<String, Object>();

        GraphQlQueryRequest request = new GraphQlQueryRequest(
                "query users {"
                        + "   users {"
                        + "     id "
                        + "     firstName "
                        + "     lastName "
                        + "     age "
                        + "     address "
                        + "     country "
                        + "     city "
                        + "     bank { "
                        + "       id "
                        + "     }"
                        + "     accounts { "
                        + "       id "
                        + "     }"
                        + "  }"
                        + "}",
                variables);

        Jsonb jsonb = JsonbBuilder.create();
        String jsonString = jsonb.toJson(request);

        given()
                .contentType(ContentType.JSON)
                .body(jsonString)
                .log().body()
                .post("/graphql")
                .then()
                .log().body()
                .statusCode(200)
                .root("data.users")
                .body("id", containsInAnyOrder(user.getId().toString(), user1.getId().toString()))
                .body("firstName", containsInAnyOrder(user.getFirstName(), user1.getFirstName()));
    }

    @Test
    public void user() {

        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("userId", user.getId());

        GraphQlQueryRequest request = new GraphQlQueryRequest(
                "query user($userId: String) {"
                        + "   user(userId: $userId) {"
                        + "     id "
                        + "     firstName "
                        + "     lastName "
                        + "     age "
                        + "     address "
                        + "     country "
                        + "     city "
                        + "     bank { "
                        + "       id "
                        + "     }"
                        + "     accounts { "
                        + "       id "
                        + "     }"
                        + "  }"
                        + "}",
                variables);

        Jsonb jsonb = JsonbBuilder.create();
        String jsonString = jsonb.toJson(request);

        given()
                .contentType(ContentType.JSON)
                .body(jsonString)
                .log().body()
                .post("/graphql")
                .then()
                .log().body()
                .statusCode(200)
                .root("data.user")
                .body("id", is(user.getId().toString()))
                .body("firstName", is(user.getFirstName()))
                .body("lastName", is(user.getLastName()));
    }

}