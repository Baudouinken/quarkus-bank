package org.acme.graphql.queries;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import jakarta.inject.Inject;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.transaction.Transactional;
import org.acme.ItBase;
import org.acme.graphql.endpoint.GraphQlQueryRequest;
import org.acme.models.Account;
import org.acme.models.Bank;
import org.acme.models.User;
import org.acme.repositories.AccountRepository;
import org.acme.repositories.BankRepository;
import org.acme.repositories.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsInAnyOrder;

@QuarkusTest
public class QueriesAccountIT extends ItBase {

    private Account account, account1;
    private User user;
    private Bank bank;

    @Inject
    private AccountRepository accountRepository;

    @Inject
    private BankRepository bankRepository;

    @Inject
    private UserRepository userRepository;

    @BeforeEach
    @Transactional
    public void setup() {

        bank = buildBank();
        bankRepository.persist(bank);

        user = buildUser();
        userRepository.persist(user);

        account = buildAccount(bank, user);
        account1 = buildAccount(bank, user);
        accountRepository.persist(account, account1);
    }

    @AfterEach
    @Transactional
    public void cleanUp() {
        accountRepository.deleteAll();
        bankRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void accounts() {

        Map<String, Object> variables = new HashMap<String, Object>();

        GraphQlQueryRequest request = new GraphQlQueryRequest(
                "query accounts {"
                        + "   accounts {"
                        + "     id "
                        + "     name "
                        + "     alias "
                        + "     amount "
                        + "     user { "
                        + "       id "
                        + "     }"
                        + "     bank { "
                        + "       id "
                        + "     }"
                        + "  }"
                        + "}",
                variables);

        Jsonb jsonb = JsonbBuilder.create();
        String jsonString = jsonb.toJson(request);

        given()
                .contentType(ContentType.JSON)
                .body(jsonString)
                .log().body()
                .post("/graphql")
                .then()
                .log().body()
                .statusCode(200)
                .root("data.accounts")
                .body("id", containsInAnyOrder(account.getId().toString(), account1.getId().toString()))
                .body("name", containsInAnyOrder(account.getName(), account1.getName()));
    }

    @Test
    public void account() {

        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("accountId", account.getId());

        GraphQlQueryRequest request = new GraphQlQueryRequest(
                "query account($accountId: String) {"
                        + "   account(accountId: $accountId) {"
                        + "     id "
                        + "     name "
                        + "     alias "
                        + "     amount "
                        + "     user { "
                        + "       id "
                        + "     }"
                        + "     bank { "
                        + "       id "
                        + "     }"
                        + "  }"
                        + "}",
                variables);

        Jsonb jsonb = JsonbBuilder.create();
        String jsonString = jsonb.toJson(request);

        given()
                .contentType(ContentType.JSON)
                .body(jsonString)
                .log().body()
                .post("/graphql")
                .then()
                .log().body()
                .statusCode(200)
                .root("data.account")
                .body("id", is(account.getId().toString()))
                .body("name", is(account.getName()))
                .body("alias", is(account.getAlias()))
                .body("amount.intValue()", is(account.getAmount().intValue()))
                .body("user.id", is(account.getUser().getId().toString()))
                .body("bank.id", is(account.getBank().getId().toString()));
    }

}