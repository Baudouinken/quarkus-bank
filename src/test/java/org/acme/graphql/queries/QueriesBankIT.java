package org.acme.graphql.queries;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import jakarta.inject.Inject;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.transaction.Transactional;
import org.acme.ItBase;
import org.acme.graphql.endpoint.GraphQlQueryRequest;
import org.acme.models.Bank;
import org.acme.repositories.BankRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsInAnyOrder;

@QuarkusTest
public class QueriesBankIT extends ItBase {

    private Bank bank, bank1;

    @Inject
    private BankRepository bankRepository;


    @BeforeEach
    @Transactional
    public void setup() {

        bank = buildBank();
        bank1 = buildBank();
        bankRepository.persist(bank, bank1);
    }

    @AfterEach
    @Transactional
    public void cleanUp() {
        bankRepository.deleteAll();
    }

    @Test
    public void banks() {

        Map<String, Object> variables = new HashMap<String, Object>();

        GraphQlQueryRequest request = new GraphQlQueryRequest(
                "query banks {"
                        + "   banks {"
                        + "     id "
                        + "     name "
                        + "     country "
                        + "     users { "
                        + "       id "
                        + "     }"
                        + "     accounts { "
                        + "       id "
                        + "     }"
                        + "  }"
                        + "}",
                variables);

        Jsonb jsonb = JsonbBuilder.create();
        String jsonString = jsonb.toJson(request);

        given()
                .contentType(ContentType.JSON)
                .body(jsonString)
                .log().body()
                .post("/graphql")
                .then()
                .log().body()
                .statusCode(200)
                .root("data.banks")
                .body("id", containsInAnyOrder(bank.getId().toString(), bank1.getId().toString()))
                .body("name", containsInAnyOrder(bank.getName(), bank1.getName()));
    }

    @Test
    public void bank() {

        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("bankId", bank.getId());

        GraphQlQueryRequest request = new GraphQlQueryRequest(
                "query bank($bankId: String) {"
                        + "   bank(bankId: $bankId) {"
                        + "     id "
                        + "     name "
                        + "     country "
                        + "     users { "
                        + "       id "
                        + "     }"
                        + "     accounts { "
                        + "       id "
                        + "     }"
                        + "  }"
                        + "}",
                variables);

        Jsonb jsonb = JsonbBuilder.create();
        String jsonString = jsonb.toJson(request);

        given()
                .contentType(ContentType.JSON)
                .body(jsonString)
                .log().body()
                .post("/graphql")
                .then()
                .log().body()
                .statusCode(200)
                .root("data.bank")
                .body("id", is(bank.getId().toString()))
                .body("name", is(bank.getName()))
                .body("country", is(bank.getCountry()));
    }

}