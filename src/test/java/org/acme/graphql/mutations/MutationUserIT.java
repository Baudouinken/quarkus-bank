package org.acme.graphql.mutations;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import jakarta.inject.Inject;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.transaction.Transactional;
import org.acme.ItBase;
import org.acme.graphql.endpoint.GraphQlQueryRequest;
import org.acme.models.User;
import org.acme.repositories.UserRepository;
import org.acme.transfer.user.UserInput;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@QuarkusTest
public class MutationUserIT extends ItBase {

    private User user;

    @Inject
    private UserRepository userRepository;

    @BeforeEach
    @Transactional
    public void setup() {
        user = buildUser();
        userRepository.persist(user);
    }

    @AfterEach
    @Transactional
    public void cleanUp() {
        userRepository.deleteAll();
    }

    @Test
    public void createUser() {

        UserInput input = buildUserInput();

        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("user", input);

        GraphQlQueryRequest request = new GraphQlQueryRequest(
                "mutation createUser($user: UserInputInput) {"
                        + "   createUser(user: $user) {"
                        + "     id "
                        + "     firstName "
                        + "     lastName "
                        + "     age "
                        + "     address "
                        + "     country "
                        + "     city "
                        + "  }"
                        + "}",
                variables);

        Jsonb jsonb = JsonbBuilder.create();
        String jsonString = jsonb.toJson(request);

        UUID id = UUID.fromString(given()
                .contentType(ContentType.JSON)
                .body(jsonString)
                .log().body()
                .post("/graphql")
                .then()
                .log().body()
                .statusCode(200)
                .extract()
                .body().path("data.createUser.id"));

        User user = userRepository.findById(id);
        assertThat(user.getFirstName(), is(input.getFirstName()));
        assertThat(user.getLastName(), is(input.getLastName()));
        assertThat(user.getAge(), is(input.getAge()));
        assertThat(user.getAddress(), is(input.getAddress()));
        assertThat(user.getCountry(), is(input.getCountry()));
        assertThat(user.getCity(), is(input.getCity()));
    }

    @Test
    public void updateUser() {

        UserInput input = buildUserInput();
        input.setId(user.getId());

        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("user", input);

        GraphQlQueryRequest request = new GraphQlQueryRequest(
                "mutation updateUser($user: UserInputInput) {"
                        + "   updateUser(user: $user) {"
                        + "     id "
                        + "     firstName "
                        + "     lastName "
                        + "     age "
                        + "     address "
                        + "     country "
                        + "     city "
                        + "  }"
                        + "}",
                variables);

        Jsonb jsonb = JsonbBuilder.create();
        String jsonString = jsonb.toJson(request);

        UUID id = UUID.fromString(given()
                .contentType(ContentType.JSON)
                .body(jsonString)
                .log().body()
                .post("/graphql")
                .then()
                .log().body()
                .statusCode(200)
                .extract()
                .body().path("data.updateUser.id"));

        User userUpdated = userRepository.findById(id);
        assertThat(userUpdated.getFirstName(), is(input.getFirstName()));
        assertThat(userUpdated.getLastName(), is(input.getLastName()));
        assertThat(userUpdated.getAge(), is(input.getAge()));
        assertThat(userUpdated.getAddress(), is(input.getAddress()));
        assertThat(userUpdated.getCountry(), is(input.getCountry()));
        assertThat(userUpdated.getCity(), is(input.getCity()));
    }

    @Test
    public void deleteUser() {

        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("userId", user.getId());

        GraphQlQueryRequest request = new GraphQlQueryRequest(
                "mutation deleteUser($userId: String) {"
                        + "   deleteUser(userId: $userId) "
                        + "}",
                variables);

        Jsonb jsonb = JsonbBuilder.create();
        String jsonString = jsonb.toJson(request);

        UUID id = UUID.fromString(given()
                .contentType(ContentType.JSON)
                .body(jsonString)
                .log().body()
                .post("/graphql")
                .then()
                .log().body()
                .statusCode(200)
                .extract()
                .body().path("data.deleteUser"));

        assertThat(userRepository.findByIdOptional(id).isPresent(), is(false));
    }

}