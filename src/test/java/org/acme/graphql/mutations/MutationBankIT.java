package org.acme.graphql.mutations;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import jakarta.inject.Inject;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.transaction.Transactional;
import org.acme.ItBase;
import org.acme.graphql.endpoint.GraphQlQueryRequest;
import org.acme.models.Bank;
import org.acme.repositories.BankRepository;
import org.acme.transfer.bank.BankInput;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@QuarkusTest
public class MutationBankIT extends ItBase {

    private Bank bank;

    @Inject
    private BankRepository bankRepository;

    @BeforeEach
    @Transactional
    public void setup() {
        bank = buildBank();
        bankRepository.persist(bank);
    }

    @AfterEach
    @Transactional
    public void cleanUp() {
        bankRepository.deleteAll();
    }

    @Test
    public void createBank() {

        BankInput input = buildBankInput();

        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("bank", input);

        GraphQlQueryRequest request = new GraphQlQueryRequest(
                "mutation createBank($bank: BankInputInput) {"
                        + "   createBank(bank: $bank) {"
                        + "     id "
                        + "     name "
                        + "     country "
                        + "  }"
                        + "}",
                variables);

        Jsonb jsonb = JsonbBuilder.create();
        String jsonString = jsonb.toJson(request);

        UUID id = UUID.fromString(given()
                .contentType(ContentType.JSON)
                .body(jsonString)
                .log().body()
                .post("/graphql")
                .then()
                .log().body()
                .statusCode(200)
                .extract()
                .body().path("data.createBank.id"));

        Bank bank = bankRepository.findById(id);
        assertThat(bank.getName(), is(input.getName()));
        assertThat(bank.getCountry(), is(input.getCountry()));
    }

    @Test
    public void updateBank() {

        BankInput input = buildBankInput();
        input.setId(bank.getId());

        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("bank", input);

        GraphQlQueryRequest request = new GraphQlQueryRequest(
                "mutation updateBank($bank: BankInputInput) {"
                        + "   updateBank(bank: $bank) {"
                        + "     id "
                        + "     name "
                        + "     country "
                        + "  }"
                        + "}",
                variables);

        Jsonb jsonb = JsonbBuilder.create();
        String jsonString = jsonb.toJson(request);

        UUID id = UUID.fromString(given()
                .contentType(ContentType.JSON)
                .body(jsonString)
                .log().body()
                .post("/graphql")
                .then()
                .log().body()
                .statusCode(200)
                .extract()
                .body().path("data.updateBank.id"));

        Bank bankUpdated = bankRepository.findById(id);
        assertThat(bankUpdated.getName(), is(input.getName()));
        assertThat(bankUpdated.getCountry(), is(input.getCountry()));
    }

    @Test
    public void deleteBank() {

        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("bankId", bank.getId());

        GraphQlQueryRequest request = new GraphQlQueryRequest(
                "mutation deleteBank($bankId: String) {"
                        + "   deleteBank(bankId: $bankId) "
                        + "}",
                variables);

        Jsonb jsonb = JsonbBuilder.create();
        String jsonString = jsonb.toJson(request);

        UUID id = UUID.fromString(given()
                .contentType(ContentType.JSON)
                .body(jsonString)
                .log().body()
                .post("/graphql")
                .then()
                .log().body()
                .statusCode(200)
                .extract()
                .body().path("data.deleteBank"));

        assertThat(bankRepository.findByIdOptional(id).isPresent(), is(false));
    }

}