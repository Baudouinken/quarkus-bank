package org.acme.graphql.mutations;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import jakarta.inject.Inject;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.transaction.Transactional;
import org.acme.ItBase;
import org.acme.graphql.endpoint.GraphQlQueryRequest;
import org.acme.models.Account;
import org.acme.models.Bank;
import org.acme.models.User;
import org.acme.repositories.AccountRepository;
import org.acme.repositories.BankRepository;
import org.acme.repositories.UserRepository;
import org.acme.transfer.account.AccountInput;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@QuarkusTest
public class MutationAccountIT extends ItBase {

    private Account account;
    private User user;
    private Bank bank;

    @Inject
    private AccountRepository accountRepository;

    @Inject
    private BankRepository bankRepository;

    @Inject
    private UserRepository userRepository;

    @BeforeEach
    @Transactional
    public void setup() {

        bank = buildBank();
        bankRepository.persist(bank);

        user = buildUser();
        userRepository.persist(user);

        account = buildAccount(bank, user);
        accountRepository.persist(account);
    }

    @AfterEach
    @Transactional
    public void cleanUp() {
        accountRepository.deleteAll();
        bankRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void createAccount() {

        AccountInput input = buildAccountInput(bank.getId(), user.getId());

        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("account", input);

        GraphQlQueryRequest request = new GraphQlQueryRequest(
                "mutation createAccount($account: AccountInputInput) {"
                        + "   createAccount(account: $account) {"
                        + "     id "
                        + "     name "
                        + "     alias "
                        + "     amount "
                        + "     user { "
                        + "       id "
                        + "     }"
                        + "     bank { "
                        + "       id "
                        + "     }"
                        + "  }"
                        + "}",
                variables);

        Jsonb jsonb = JsonbBuilder.create();
        String jsonString = jsonb.toJson(request);

        UUID id = UUID.fromString(given()
                .contentType(ContentType.JSON)
                .body(jsonString)
                .log().body()
                .post("/graphql")
                .then()
                .log().body()
                .statusCode(200)
                .extract()
                .body().path("data.createAccount.id"));

        Account account = accountRepository.findById(id);
        assertThat(account.getName(), is(input.getName()));
        assertThat(account.getAlias(), is(input.getAlias()));
        assertThat(account.getAmount().intValue(), is(input.getAmount().intValue()));
        assertThat(account.getUser().getId(), is(user.getId()));
        assertThat(account.getBank().getId(), is(bank.getId()));
    }

    @Test
    public void updateAccount() {

        AccountInput input = buildAccountInput(bank.getId(), user.getId());
        input.setId(account.getId());

        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("account", input);

        GraphQlQueryRequest request = new GraphQlQueryRequest(
                "mutation updateAccount($account: AccountInputInput) {"
                        + "   updateAccount(account: $account) {"
                        + "     id "
                        + "     name "
                        + "     alias "
                        + "     amount "
                        + "     user { "
                        + "       id "
                        + "     }"
                        + "     bank { "
                        + "       id "
                        + "     }"
                        + "  }"
                        + "}",
                variables);

        Jsonb jsonb = JsonbBuilder.create();
        String jsonString = jsonb.toJson(request);

        UUID id = UUID.fromString(given()
                .contentType(ContentType.JSON)
                .body(jsonString)
                .log().body()
                .post("/graphql")
                .then()
                .log().body()
                .statusCode(200)
                .extract()
                .body().path("data.updateAccount.id"));

        Account accountUpdated = accountRepository.findById(id);
        assertThat(accountUpdated.getName(), is(input.getName()));
        assertThat(accountUpdated.getAlias(), is(input.getAlias()));
        assertThat(accountUpdated.getAmount().intValue(), is(input.getAmount().intValue()));
        assertThat(accountUpdated.getUser().getId(), is(user.getId()));
        assertThat(accountUpdated.getBank().getId(), is(bank.getId()));
    }

    @Test
    public void deleteAccount() {

        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("accountId", account.getId());

        GraphQlQueryRequest request = new GraphQlQueryRequest(
                "mutation deleteAccount($accountId: String) {"
                        + "   deleteAccount(accountId: $accountId) "
                        + "}",
                variables);

        Jsonb jsonb = JsonbBuilder.create();
        String jsonString = jsonb.toJson(request);

        UUID id = UUID.fromString(given()
                .contentType(ContentType.JSON)
                .body(jsonString)
                .log().body()
                .post("/graphql")
                .then()
                .log().body()
                .statusCode(200)
                .extract()
                .body().path("data.deleteAccount"));

        assertThat(accountRepository.findByIdOptional(id).isPresent(), is(false));
    }

}